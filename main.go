package main

import (
	"fmt"
	"sort"
	"strings"
	"unicode/utf8"
)

func sortByCountOfA(arr []string) {
	sort.Slice(arr, func(i, j int) bool { //custom sort
		s1, s2 := arr[i], arr[j]
		count1, count2 := strings.Count(s1, "a"), strings.Count(s2, "a")
		if count1 != count2 {
			return count1 > count2 //to sort count of a's desc
		}
		return utf8.RuneCountInString(s1) > utf8.RuneCountInString(s2) //compare alphabetically if #a equal
	})

	fmt.Println(arr)

}

func takeHalfRecursively(n int) {

	if n == 0 || n == 1 {
		return
	}
	fmt.Println(n)
	takeHalfRecursively(n / 2)

}

func mostFrequent(arr []string) string { // assuming no tie

	freq := make(map[string]int) // frequency map
	for _, elem := range arr {
		freq[elem] = freq[elem] + 1 // update frequency of current element
	}
	var maxFreq = 0
	var mostFreqElem string
	for key, element := range freq { // find the max value's key (most frequent element)
		if element > maxFreq {
			maxFreq = element
			mostFreqElem = key
		}
	}
	return mostFreqElem
}

func main() {

	fmt.Println("")
	fmt.Println("---------------Answer 1---------------")
	q1 := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	sortByCountOfA(q1)

	fmt.Println("")
	fmt.Println("---------------Answer 2---------------")
	takeHalfRecursively(9)

	fmt.Println("")
	fmt.Println("---------------Answer 3---------------")
	q3 := []string{"apple", "pie", "apple", "red", "red", "red"}
	fmt.Println(mostFrequent(q3))

}
